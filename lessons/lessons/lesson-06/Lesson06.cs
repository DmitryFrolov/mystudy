﻿using System;
using System.Linq;
using System.Reflection.PortableExecutable;

namespace lessons.lesson06
{
    public class Lesson06
    {


        public void Run()
        {
            

            
            //ValueTypeContainingRefType();
            //Console.ReadLine();
               
            bool? b = null; // true, false, null

            bool c = b ?? true; // true, false;

            if (b == null)
                c = true;
            else
            {
                c = b.Value;
            }


            int? i = null;
            float f = default; //0.0

            string str = "sdfdsf"; //sdfdsf
            string str2 = str;  //sdfdsf

            str = "new"; //new

            Console.WriteLine(str); //
            Console.WriteLine(str2);

        }

        #region Struct containing class!

        public class ShapeInfo
        {
            public string InfoString;

            public ShapeInfo(string info)
            {
                InfoString = info;
            }
        }

        public class Rectangle
        {
            // The Rectangle structure contains a reference type member.
            public ShapeInfo RectInfo;

            public int RectTop, RectLeft, RectBottom, RectRight;

            public Rectangle(string info, int top, int left, int bottom, int right)
            {
                RectInfo = new ShapeInfo(info);
                RectTop = top; 
                RectBottom = bottom;
                RectLeft = left; 
                RectRight = right;
            }

            public Rectangle()
            {
                
            }

            public void Display()
            { 
                Console.WriteLine("String = {0}, Top = {1}, Bottom = {2}, " +
                                  "Left = {3}, Right = {4}",
                    RectInfo.InfoString, RectTop, RectBottom, RectLeft, RectRight);
            }
        }

        #endregion

        #region Value types containing ref types
        static void ValueTypeContainingRefType()
        {
            // Create the first Rectangle.
            Console.WriteLine("-> Creating r1");
            Rectangle r1 = new Rectangle();

            // Now assign a new Rectangle to r1.
            Console.WriteLine("-> Assigning r2 to r1");
            Rectangle r2 = r1;

            // Change some values of r2.
            Console.WriteLine("-> Changing values of r2");
            r2.RectInfo.InfoString = "This is new info!";
            r2.RectBottom = 4444;

            // Print values of both rectangles.
            r1.Display();
            r2.Display();
        }
        #endregion

        #region Simple Person class

        class Person
        {
            public string personName;
            public int personAge;

            // Constructors.
            public Person(string name, int age)
            {
                personName = name;
                personAge = age;
            }

            public void Display()
            {
                Console.WriteLine("Name: {0}, Age: {1}", personName, personAge);
            }
        }

        #endregion

        #region Helper functions / modifiers

        static void SendAPersonByValue(Person p)
        {
            // Change the age of "p"?
            p.personAge = 99;

            // Will the caller see this reassignment?
            p = new Person("Nikki", 99);
        }

        static void SendAPersonByReference(ref Person p)
        {
            // Change some data of "p".
            p.personAge = 555;

            // "p" is now pointing to a new object on the heap!
            p = new Person("Nikki", 999);
        }

        static void UseOutParamModifier(out Person person)
        {
            person = new Person("Nikki", 999);
        }

        static void UserParamsModifier(params Person[] persons)
        {
            for (int index = 0; index < persons.Length; index++)
            {
                Console.WriteLine(persons[index]);
            }
        }

        #endregion

        
        static void Func(int a, int b)
        {
            a = 10;
            b = 20;
        }

        static void Func2(ref Person p)
        {
            p.personName = "new Name";
            p.personAge = 20;

            p = new Person("sdf", 54);
        }

        static void Func3(out Person p)
        {
            p = new Person("name", 50);
        }

        static void Func4(params Person[] p)
        {
            for (int index = 0; index < p.Length; index++)
            {
                p[index].Display();
            }
        }

        static void Func5(int a, int b)
        {

        }
    }
}
