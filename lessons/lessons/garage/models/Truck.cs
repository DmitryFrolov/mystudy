﻿using System;

namespace lessons.garage
{
    public class Truck : Car
    {
        public Truck(string _id = "AA 2222 AA", string _type = "Truck", int _weight =4900 ,  int _mileage = 148000, int _refuel=30, int _fuelTankVolume=250,int _carryingCapacity = 5500)
            : base( _id,  _type,  _weight, _mileage, _refuel, _fuelTankVolume)
        {
            CarryingCapacity = _carryingCapacity;
        }
        public int CarryingCapacity { get; set; }

        protected override int NextTechnicalServise()
        {
            bool indicator = false;
            int servise = Mileage;
            string m = Mileage.ToString();

            if (m.Length >= 4 && servise >= 8000)
            {
                do
                {
                    servise = servise - 8000;
                    string value = servise.ToString();
                    if (value.Length < 4)
                    {
                        indicator = true;

                        if (servise <= 0)
                            servise = 0;
                        else
                            servise = 8000 - servise;
                    }
                } while (indicator == false);
            }
            else
                servise = 8000 - servise;


            return servise;
        }

        public override void DisplayInfo()
        {
            base.DisplayInfo();
            Console.WriteLine($"Carrying capacity: {CarryingCapacity}");
            Console.WriteLine($"Next technical servise after: {NextTechnicalServise()}");

            if(NeedToRefuelCar() == true)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("You need to refuel the car");
                Console.ForegroundColor = ConsoleColor.Gray;
            }

            Console.WriteLine();

        }
    }
}
