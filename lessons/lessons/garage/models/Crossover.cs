﻿using System;

namespace lessons.garage
{
    public class Crossover : Car
    {
        public Crossover(string _id = "AA 0000 AA", string _type = "Crossover", int _weight =1700 , int _mileage = 93000, int _refuel=10, int _fuelTankVolume=60, int _carryingCapacity = 650)
            : base( _id,  _type,  _weight, _mileage, _refuel, _fuelTankVolume)
        {            
            CarryingCapacity = _carryingCapacity;
        }
        public int CarryingCapacity { get; set;}

        protected override int NextTechnicalServise()
        {
            bool indicator = false;
            int servise = Mileage;
            string m = Mileage.ToString();

            if (m.Length >= 5 && servise>=15000)
            {
                do
                {
                    servise = servise - 15000;
                    string value = servise.ToString();
                    if (value.Length < 5)
                    {
                        indicator = true;

                        if (servise <= 0)
                            servise = 0;
                        else
                            servise = 15000 - servise;
                    }
                } while (indicator == false);
            }
            else
                servise = 15000 - servise;


            return servise;
        }

        protected override bool NeedToRefuelCar()
        {
            if (FuelTankVolum - Refuel >= FuelTankVolum - 10)
                return true;
            else
                return false;
        }

        public override void DisplayInfo()
        {
            base.DisplayInfo();
            Console.WriteLine($"Carrying capacity: {CarryingCapacity}");
            Console.WriteLine($"Next technical servise after: {NextTechnicalServise()}");

            if(NeedToRefuelCar() == true)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("You need to refuel the car");
                Console.ForegroundColor = ConsoleColor.Gray;
            }

            Console.WriteLine();

        }
    }
}
