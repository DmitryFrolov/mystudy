﻿using lessons.garage.Interfaces;
using System;

namespace lessons.garage
{
    public abstract class Car: Displayed
    {
        protected Car(string _id, string _type,int _weight,  int _mileage, int _refuel,int _fuelTankVolume)
        {
            ID = _id;
            Type = _type;
            Weight = _weight;
            Mileage = _mileage;
            FuelTankVolum = _fuelTankVolume;
            Refuel = _refuel;

        }
        private int refuelValue;
        private int mileagelValue;
        private int fuelTankVolumValue;
        private int weightValue;


        public string ID { get; set; }
        public string Type { get; set;}
        public int Weight
        {
            get
            {
                return weightValue;
            }
            set
            {
                if (value < 0)
                    weightValue = 0;
                else
                    weightValue = value;
            }
        }
        public int FuelTankVolum
        {
            get
            {
                return fuelTankVolumValue;
            }
            set
            {
                if (value < 0)
                    fuelTankVolumValue = 0;
                else
                    fuelTankVolumValue = value;
            }
        }
        public int Mileage
        {
            get
            {
                return mileagelValue;
            }
            set
            {
                if (value < 0)
                    mileagelValue = 0;
                else
                    mileagelValue = value;
            }
        }
        public int Refuel
        {
            get
            {
                return refuelValue;
            }
            set 
            {
                if (value <= FuelTankVolum)
                    refuelValue = value;
                else if (value < 0)
                    refuelValue = 0;
                else
                    refuelValue = FuelTankVolum;
            }
        }

        protected virtual bool NeedToRefuelCar()
        {
            if (FuelTankVolum - Refuel >= FuelTankVolum - 30)
                return true;
            else
                return false;
        }
        protected abstract int NextTechnicalServise();

        public virtual void DisplayInfo()
        {
            Console.WriteLine($"ID: {ID}");
            Console.WriteLine($"Type of car: {Type}");
            Console.WriteLine($"Weight of car (kg): {Weight}");
            Console.WriteLine($"Mileage (km): {Mileage}");
            Console.WriteLine($"Refuel (L): {Refuel}");
        }

    }
}
