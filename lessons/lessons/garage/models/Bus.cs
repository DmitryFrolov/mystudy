﻿using System;

namespace lessons.garage
{
    public class Bus : Car
    {
        public Bus(string _id = "AA 1111 AA", string _type = "Bus", int _weight =5000 ,  int _mileage = 157000, int _refuel=30, int _fuelTankVolume=250,int _passengerSeats = 55)
            : base( _id,  _type,  _weight, _mileage, _refuel, _fuelTankVolume)
        {
            PassengerSeats = _passengerSeats;
        }
        public int PassengerSeats { get; set;}

        protected override int NextTechnicalServise()
        {
            bool indicator = false;
            int servise = Mileage;
            string m = Mileage.ToString();

            if (m.Length >= 5 && servise >= 10000)
            {
                do
                {
                    servise = servise - 10000;
                    string value = servise.ToString();
                    if (value.Length < 5)
                    {
                        indicator = true;

                        if (servise <= 0)
                            servise = 0;
                        else
                            servise = 10000 - servise;
                    }
                } while (indicator == false);
            }
            else
                servise = 10000 - servise;


            return servise;
        }

        public override void DisplayInfo()
        {
            base.DisplayInfo();
            Console.WriteLine($"Passenger Seats: {PassengerSeats}");
            Console.WriteLine($"Next technical servise after: {NextTechnicalServise()}");

            if(NeedToRefuelCar() == true)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("You need to refuel the car");
                Console.ForegroundColor = ConsoleColor.Gray;
            }

            Console.WriteLine();

        }
    }
}
