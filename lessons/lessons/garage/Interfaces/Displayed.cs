﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lessons.garage.Interfaces
{
    interface Displayed
    {
        void DisplayInfo();
    }
}
