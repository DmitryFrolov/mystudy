﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lessons.garage
{
    public static class CarFactory
    {
        public static Car CreateCar()
        {
            Console.WriteLine(" Выберите тип авто:\n    1  Кроссовер\n    2  Автобус\n    3  Грузовик");
            string date = CarsControlInterfase.GetData();
            switch (date)
            {
                case "1":
                    Console.WriteLine(@"Введите данные авто в таком порядке:
    Гос номер
    Модель авто
    Вес авто
    Пробег
    Колличество бензина в баке
    Объем топливного бака
    Грузоподъемность");
                    Crossover crossover = new Crossover(CarsControlInterfase.GetData().ToUpper(), CarsControlInterfase.GetData(), Convert.ToInt32(CarsControlInterfase.GetData()), Convert.ToInt32(CarsControlInterfase.GetData()), Convert.ToInt32(CarsControlInterfase.GetData()), Convert.ToInt32(CarsControlInterfase.GetData()), Convert.ToInt32(CarsControlInterfase.GetData()));
                    return crossover;

                case "2":
                    Console.WriteLine(@"Введите данные авто в таком порядке:
    Гос номер
    Модель авто
    Вес авто    
    Пробег
    Количество бензина в баке
    Объем топливного бака
    Количество пассажирских мест");
                    
                    
                    
                    
                    Bus bus = new Bus(CarsControlInterfase.GetData().ToUpper(), CarsControlInterfase.GetData(), Convert.ToInt32(CarsControlInterfase.GetData()), Convert.ToInt32(CarsControlInterfase.GetData()), Convert.ToInt32(CarsControlInterfase.GetData()), Convert.ToInt32(CarsControlInterfase.GetData()), Convert.ToInt32(CarsControlInterfase.GetData()));
                    return bus;

                case "3":
                    Console.WriteLine(@"Введите данные авто в таком порядке:
    Гос номер
    Модель авто
    Вес авто
    Пробег
    Колличество бензина в баке
    Объем топливного бака
    Грузоподъемность");
                    Truck truck = new Truck(CarsControlInterfase.GetData().ToUpper(), CarsControlInterfase.GetData(), Convert.ToInt32(CarsControlInterfase.GetData()), Convert.ToInt32(CarsControlInterfase.GetData()), Convert.ToInt32(CarsControlInterfase.GetData()), Convert.ToInt32(CarsControlInterfase.GetData()), Convert.ToInt32(CarsControlInterfase.GetData()));
                    return truck;
                default:
                    Crossover avto = new Crossover();
                    return avto;

            }
        }

        public static Car fakeAvtoCreator()
        {
            Console.WriteLine(" Выберите тип авто:\n    1  Кроссовер\n    2  Автобус\n    3  Грузовик");
            string date = CarsControlInterfase.GetData();
            switch (date)
            {
                case "1":
                    Crossover crossover = new Crossover();
                    return crossover;

                case "2":
                    Bus bus = new Bus();
                    return bus;

                case "3":
                    Truck truck = new Truck();
                    return truck;
                default:
                    Crossover avto = new Crossover();
                    return avto;

            }
        }
    }
}
