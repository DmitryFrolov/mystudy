﻿using System.Collections.Generic;
using System;
using System.Linq;

namespace lessons.garage
{
    public class Garage
    {
        List<Car> parkig = new List<Car>();

        public void AddCarsToParking()
        {
            parkig.Add(CarFactory.CreateCar());

            //parkig.Add(CarFactory.fakeAvtoCreator()); // создание авто с дефолтным конструктором
        }

        public void DeleteCarsToParking(string s)
        {
            bool searchIndicator = true; 
            foreach (var car in parkig)
            {
                if (car.ID.Equals(s))
                {
                    parkig.RemoveAt(parkig.IndexOf(car));
                    Console.Clear();
                    break;
                }
                else
                    searchIndicator = false;
            }
            if (searchIndicator == false)
            {
                Console.BackgroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("Такого авто нет на парковке");
                Console.BackgroundColor = ConsoleColor.Black;
            }
        }

        public void SearchAvto(string s)
        {
            bool searchIndicator = true;
            foreach (var car in parkig)
            {
                if (car.ID.Equals(s))
                {
                    Console.WriteLine(parkig.IndexOf(car) + 1);
                    Console.Clear();
                    car.DisplayInfo();
                    break;
                }
                else
                    searchIndicator = false;
            }
            if (searchIndicator == false)
            {
                Console.BackgroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("Такого авто нет на парковке");
                Console.BackgroundColor = ConsoleColor.Black;
            }
        }

        public void ShowInfoParking()
        {
            Console.WriteLine($"Всего авто на парковке: {parkig.Count()} машин");
            Console.WriteLine();
            foreach (var item in parkig)
            {
                Console.ForegroundColor = ConsoleColor.Magenta;
                int indexItem = parkig.IndexOf(item) + 1;
                Console.WriteLine(indexItem); 
                Console.ForegroundColor = ConsoleColor.White;
                item.DisplayInfo();
                
            }
        }
    }
}
