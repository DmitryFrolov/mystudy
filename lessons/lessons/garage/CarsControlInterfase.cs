﻿using System;

namespace lessons.garage
{
    public static class CarsControlInterfase
    {
        public static void Run()
        {
            Console.ForegroundColor = ConsoleColor.White;

            Interfase();

            Console.ReadKey();
        }

        public static void Interfase()
        {
            Garage garage = new Garage();
            do
            {
                Console.WriteLine("Вудите код операции, которую совершить сделать. \nВведите help, чтобы посмотреть доступные операции");
                Console.WriteLine();
                string date = GetData().ToLower();
                switch (date)
                {
                    case "help":
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine(@"
    ‘+’ - Добавить    авто    на    парковку , 
    ‘-’ - Убрать     авто     с     парковки , 
    ‘i’ - Информация о всех авто на парковке , 
    ‘f’ - Поиск      по      номеру     авто ,
    ‘c’ - Очистить консоль ");
                        Console.ForegroundColor = ConsoleColor.White;
                        break;
                    case "+":
                        garage.AddCarsToParking();
                        Console.Clear();
                        break;
                    case "-":
                        Console.WriteLine("Ведите  гос. номер удаляемого авто");
                        garage.DeleteCarsToParking(GetData().ToUpper());
                        break;
                    case "i":
                        garage.ShowInfoParking();
                        break;
                    case "f":
                        garage.SearchAvto(GetData().ToUpper());
                        break;
                    case "c":
                        Console.Clear();
                        break;
                    default:
                        Console.WriteLine("Не верный код операции.");
                        break;
                }
            } while (true);
        }
        
        public static string GetData()
        {
            string date = Console.ReadLine();
            return date;
        }

        //public static void DynamicMenu()
        //{
        //    string[] menuElement = new string[] {
        //        "Информация о всех авто на парковке ", 
        //        "Добавить    авто    на    парковку ",
        //        "Убрать     авто     с     парковки ",
        //        "Поиск      по      номеру     авто ",
        //    };


        //    Console.BackgroundColor = ConsoleColor.DarkGreen;
        //    Console.ForegroundColor = ConsoleColor.White;
        //    Console.WriteLine(menuElement[0]);
        //    Console.WriteLine();

        //    Console.BackgroundColor = ConsoleColor.Gray;
        //    Console.ForegroundColor = ConsoleColor.Black;
        //    Console.WriteLine(menuElement[1]);
        //    Console.WriteLine();
        //    Console.WriteLine(menuElement[2]);
        //    Console.WriteLine();
        //    Console.WriteLine(menuElement[3]);
        //    GetKey();
        //    Console.Clear();
        //    switch (GetKey())
        //    {
        //        case ConsoleKey.UpArrow:
        //            Console.BackgroundColor = ConsoleColor.Gray;
        //            Console.ForegroundColor = ConsoleColor.Black;

        //            Console.WriteLine(menuElement[0]);
        //            Console.WriteLine();
        //            Console.WriteLine(menuElement[1]);
        //            Console.WriteLine();
        //            Console.WriteLine(menuElement[2]);
        //            Console.WriteLine();

        //            Console.BackgroundColor = ConsoleColor.DarkGreen;
        //            Console.ForegroundColor = ConsoleColor.White;
        //            Console.WriteLine(menuElement[3]);
        //            Console.Clear();
        //            break;

        //    }
        //    Console.BackgroundColor = ConsoleColor.Gray;
        //    Console.ForegroundColor = ConsoleColor.Black;



        //}

        //public static ConsoleKey GetKey()
        //{
        //    ConsoleKeyInfo keyInfo = Console.ReadKey();
        //    ConsoleKey key = keyInfo.Key;
        //    return key;
        //}

    }
}
