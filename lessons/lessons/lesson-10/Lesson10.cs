﻿using System;
using lessons.lesson_10.Models;

namespace lessons.lesson_10
{
    public static class Lesson10
    {
        public static void Run()
        {
            Console.WriteLine("***** The Employee Class Hierarchy *****\n");

            SalesPerson fran = new SalesPerson("Fran", 43, 93, 3000, "932-32-3232", 31);
            Employee chucky = new Manager("Chucky", 50, 92, 100000, "333-23-2322", 9000);
            Employee bred = new PTSalesPerson("Bred", 50, 92, 100000, "333-23-2322", 9000);

            Employee[] employees = new Employee[]
            {
                fran, bred, chucky
            };

            foreach (var employee in employees)
            {
                GiveBonusToEmployee(400, employee);
                DisplayEmployee(employee);
            }
        }

        public static void GiveBonusToEmployee(int amount, Employee employee)
        {
            employee.GiveBonus(amount);
        }

        public static void DisplayEmployee(Employee employee)
        {
            employee.DisplayStats();

            if (employee is SalesPerson salesPerson)
            {
                ShowSalesNumber(salesPerson);
                Console.WriteLine("OH that sales....");
            }
            else if (employee is Manager)
            {
                Console.WriteLine("OOOH, MANAGER");
            }
        }

        public static void ShowSalesNumber(SalesPerson salesPerson)
        {
            Console.WriteLine(salesPerson.SalesNumber);
        }
    }
}
