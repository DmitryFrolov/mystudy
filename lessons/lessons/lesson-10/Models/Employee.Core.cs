﻿using System;

namespace lessons.lesson_10.Models
{
    public abstract class Employee
    {
        protected Employee(string name, int age, int id, float pay, string ssn = "")
        {
            Name = name;
            Age = age;
            ID = id;
            Pay = pay;
            SocialSecurityNumber = ssn;
        }

        private string empName;

        public string Name
        {
            get { return empName; }
            set
            {
                if (value.Length > 15)
                    Console.WriteLine("Error! Name length exceeds 15 characters");
                else
                    empName = value;
            }
        }

        public int ID { get; set; } = 1;

        public float Pay { get; set; }

        public int Age { get; set; }

        public readonly string SocialSecurityNumber;

        public int Holidays { get; set; }

        public virtual int CalculateHolidays()
        {
            Holidays = 20;
            return Holidays;
        }

        // Expose certain benefit behaviors of object.
        public double GetBenefitCost()
        {
            return Benefits.ComputePayDeduction();
        }

        public virtual void GiveBonus(float amount)
        {
            Pay += amount;
        }

        public virtual void DisplayStats()
        {
            Console.WriteLine("Name: {0}", Name);
            Console.WriteLine("ID: {0}", ID);
            Console.WriteLine("Age: {0}", Age);
            Console.WriteLine("Pay: {0}", Pay);
            Console.WriteLine("SSN: {0}", SocialSecurityNumber);
            Console.WriteLine($"Holidays: {Holidays}");
        }

        // Expose object through a custom property.
        public BenefitPackage Benefits { get; set; } = new BenefitPackage();
    }
}
