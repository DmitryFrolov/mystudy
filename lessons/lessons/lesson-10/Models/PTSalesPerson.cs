﻿using System;

namespace lessons.lesson_10.Models
{
    internal sealed class PTSalesPerson : SalesPerson
    {
        public PTSalesPerson(string fullName, int age, int empID,
            float currPay, string ssn, int numbOfSales)
            : base(fullName, age, empID, currPay, ssn, numbOfSales)
        {
        }

        public override void DisplayStats()
        {
            Console.WriteLine($"Type: {nameof(PTSalesPerson)}");
            base.DisplayStats();
        }
    }
}