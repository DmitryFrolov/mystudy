﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using lessons.lesson_13.Exceptions;
using lessons.lesson_13.Model;

namespace lessons.lesson_13
{
    public static class Lesson13
    {
        public static void Run()
        {
            try
            {
                SimpleException();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void SimpleException()
        {
            Console.WriteLine("***** Simple Exception Example *****\n");
            Console.WriteLine("=> Creating a car and stepping on it!");
            
            Car myCar = new Car("Zippy", 0);
            myCar.CrankTunes(true);


            // Speed up past the car's max speed to
            // trigger the exception.
            try
            {
                for (int i = 0; i < 10; i++)
                    myCar.Accelerate(10);
            }
            catch (CarIsDieException e)
            {
                Console.WriteLine($"Message: {e.Message}");
                Console.WriteLine($"CurrentSpeed: {e.CurrentSpeed}");
                Console.WriteLine($"Cause: {e.Cause}");
                Console.WriteLine($"TimeStamp: {e.TimeStamp}");
            }
            catch (CarCantGoException e)
            {
                Console.WriteLine($"Message: {e.Message}");
                Console.WriteLine($"Cause: {e.Cause}");

                try
                {
                    FileStream file = File.Open("C:\\myDocs\\carFile.txt", FileMode.Open);
                }
                catch (SystemException ex)
                {
                    Console.WriteLine("exception was occured while reading the file.");
                    throw new CarCantGoException(e.Message, e.Cause, ex);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("\n*** Error! ***");
                Console.WriteLine("Member name: {0}", e.TargetSite);
                Console.WriteLine("Class defining member: {0}",
                    e.TargetSite.DeclaringType);
                Console.WriteLine("Member type: {0}", e.TargetSite.MemberType);
                Console.WriteLine("Message: {0}", e.Message);
                Console.WriteLine("Source: {0}", e.Source);
                Console.WriteLine("Stack: {0}", e.StackTrace);
                Console.WriteLine("Help Link: {0}", e.HelpLink);
                Console.WriteLine("\n-> Custom Data:");
                foreach (DictionaryEntry de in e.Data)
                    Console.WriteLine("-> {0}: {1}", de.Key, de.Value);
            }
            finally
            {
                myCar.CrankTunes(false);
            }

            //// The error has been handled, processing continues with the next statement.
            //Console.WriteLine("\n***** Out of exception logic *****");
            //Console.ReadLine();
        }

        public static void CustomException()
        {

        }
    }
}
