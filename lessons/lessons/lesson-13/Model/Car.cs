﻿using System;
using lessons.lesson_13.Exceptions;

namespace lessons.lesson_13.Model
{
    class Car
    {
        // Constant for maximum speed.
        public const int MaxSpeed = 100;

        // Car properties.
        public int CurrentSpeed { get; set; } = 0;
        public string PetName { get; set; } = "";

        // Is the car still operational?
        private bool _carIsDead;

        // A car has-a radio.
        private Radio _theMusicBox;

        public Car(string name, int speed)
        {
            CurrentSpeed = speed;
            PetName = name;

            _theMusicBox = new Radio();
        }

        public void CrankTunes(bool state)
        {
            // Delegate request to inner object.
            _theMusicBox.TurnOn(state);
        }

        // See if Car has overheated.
        public void Accelerate(int delta)
        {
            if (CurrentSpeed == 0)
            {
                Exception ex = new CarCantGoException("Car can't go", $"current speed: {CurrentSpeed}");
                throw ex;
            }

            if (_carIsDead)
                Console.WriteLine("{0} is out of order...", PetName);
            else
            {
                CurrentSpeed += delta;
                if (CurrentSpeed >= MaxSpeed)
                {
                    _carIsDead = true;
                    CurrentSpeed = 0;

                    //Exception ex = GetBaseException();

                    Exception ex = GetCarIsDieException();

                    throw ex;
                }
                else
                    Console.WriteLine("=> CurrentSpeed = {0}", CurrentSpeed);
            }
        }

        public Exception GetBaseException()
        {
            // We need to call the HelpLink property, thus we need
            // to create a local variable before throwing the Exception object.
            Exception ex = new Exception($"{PetName} has overheated!");
            ex.HelpLink = "http://www.CarsRUs.com";

            //Stuff in custom data regarding the error.
            ex.Data.Add("TimeStamp", $"The car exploded at {DateTime.Now}");
            ex.Data.Add("Cause", "You have a lead foot.");

            return ex;
        }

        public Exception GetCarIsDieException()
        {
            Exception ex = new CarIsDieException(
                $"{PetName} has overheated!", 
                CurrentSpeed, 
                "You have a lead foot.", 
                DateTime.Now);

            return ex;
        }
    }
}
