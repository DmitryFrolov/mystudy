﻿using System;

namespace lessons.lesson_13.Exceptions
{
    public class CarCantGoException : ApplicationException
    {
        public string Cause { get; set; }

        public CarCantGoException(string message, string cause, Exception ex = null)
            : base(message, ex)
        {
            Cause = cause;
        }
    }
}