﻿using System;

namespace lessons.lesson_13.Exceptions
{
    public class CarIsDieException : ApplicationException
    {
        public int CurrentSpeed { get; set; }

        public string Cause { get; set; }

        public DateTime TimeStamp { get; set; }

        public CarIsDieException(string message, int currentSpeed, string cause, DateTime time)
            : base(message)
        {
            CurrentSpeed = currentSpeed;
            Cause = cause;
            TimeStamp = time;
        }
    }
}