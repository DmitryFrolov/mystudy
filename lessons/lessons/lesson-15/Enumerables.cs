﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace lessons.lesson15
{
    public class Progression : IEnumerable<int>
    {
        private int _itemsCount; // нужна, чтобы посл-ть была конечной

        public Progression(int count)
        {
            _itemsCount = count;
        }

        public IEnumerator<int> GetEnumerator()
        {
            return new ProgressionEnumerator(_itemsCount);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    public class ProgressionEnumerator : IEnumerator<int>
    {
        private readonly int _itemsCount;
        private int _position;
        private int _current;

        public int Current => _current;
        
        object IEnumerator.Current => Current;

        public ProgressionEnumerator(int itemsCount)
        {
            _itemsCount = itemsCount;
            _current = 1;
            _position = 0;
        }

        public void Dispose()
        {
        }

        public bool MoveNext()
        {
            if (_position > 0)
                _current += 3;

            if (_position < _itemsCount)
            {
                _position++;
                return true;
            }

            return false;
        }

        public void Reset()
        {
            _position = 0;
            _current = 1;
        }
    }
}
