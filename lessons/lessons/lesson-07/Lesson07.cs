﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;
using Newtonsoft.Json;

namespace lessons.lesson_07
{
    namespace innerLesson
    {
        public class DataModel
        {

        }
    }

    namespace innerLesson2
    {
        public class DataModel
        {

        }
    }
    
    public struct Hero
    {
        private string _name;

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                    return;

                _name = value;
            }
        }

        //private string _name;
        //private string _surname;

        public string Surname { get; private set; }   

        private int _level;

        public Hero(string name, string surname, int level)
        {
            _name = name;
            Surname = surname;
            _level = level;
        }

        //public void SetName(string name)
        //{
        //    if (string.IsNullOrEmpty(name))
        //        return;
            
        //    _name = name;
        //}

        //public string GetName()
        //{
        //    return _name;
        //}

        public void Display()
        {
            Console.WriteLine($"Name: {Name}, Surname: {Surname}, Level: {_level}");
        }
    }


    


    class Lesson07
    {
        public void Run()
        {
            Hero hero = new Hero("Name", "SN", 10);

            string name = null;

            hero.Display();

            Console.WriteLine(hero.Name);
        }

         
    }
}
