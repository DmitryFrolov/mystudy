﻿using System;
using lessons.lesson_11.Models;

namespace lessons.lesson_11
{
    public static class Lesson11
    {
        public static void Run()
        {
            Console.WriteLine("***** The Employee Class Hierarchy *****\n");

            SalesPerson fran = new SalesPerson("Fran", 150, 93, 3000, "932-32-3232", 31);
            Employee chucky = new Manager("Chucky", 50, 92, 100000, "333-23-2322", 9000);
            Employee bred = new PTSalesPerson("Bred", 50, 92, 100000, "333-23-2322", 9000);

            SalesPerson fran2 = new SalesPerson("Fran", 150, 93, 3000, "932-32-3232", 31);;

            Console.WriteLine($"fran == fran2 ? :{fran2.Equals(fran)}");

            Console.WriteLine(fran.GetHashCode());
            Console.WriteLine(fran2.GetHashCode());
            Console.WriteLine(bred.GetHashCode());

            Employee[] employees = new Employee[]
            {
                fran, bred, chucky
            };

            string name = "Fran";

            Console.WriteLine(name.GetHashCode());
            Console.WriteLine(fran.GetHashCode());

            foreach (var employee in employees)
            {
                GiveBonusToEmployee(400, employee);
                DisplayEmployee(employee);
            }
        }

        public static void GiveBonusToEmployee(int amount, Employee employee)
        {
            employee.GiveBonus(amount);
        }

        public static void DisplayEmployee(Employee employee)
        {
            employee.DisplayStats();
        }

        public static void ShowSalesNumber(SalesPerson salesPerson)
        {
            Console.WriteLine(salesPerson.SalesNumber);
        }
    }
}
