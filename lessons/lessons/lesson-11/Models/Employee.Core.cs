﻿using System;

namespace lessons.lesson_11.Models
{
    public abstract class Employee : Object
    {
        protected Employee(string name, int age, int id, float pay, string ssn = "")
        {
            Name = name;
            Age = age;
            ID = id;
            Pay = pay;
            SocialSecurityNumber = ssn;
        }

        private string empName;

        public string Name
        {
            get { return empName; }
            set
            {
                if (value.Length > 15)
                    Console.WriteLine("Error! Name length exceeds 15 characters");
                else
                    empName = value;
            }
        }

        public int ID { get; } = 1;

        public float Pay { get; set; }

        private int _age;
        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                if (value > 100)
                {
                    _age = 100;
                }
                else
                {
                    _age = value;
                }
            }
        }

        public readonly string SocialSecurityNumber;

        public int Holidays { get; set; }

        public virtual int CalculateHolidays()
        {
            Holidays = 20;
            return Holidays;
        }

        public virtual void GiveBonus(float amount)
        {
            Pay += amount;
        }

        public virtual void DisplayStats()
        {
            Console.WriteLine(ToString());
            Console.WriteLine();
        }

        public override bool Equals(object obj)
        {
            if (base.Equals(obj))
            {
                return true;
            }
            else
            {
                if (obj is Employee employee
                    && employee.GetHashCode() == GetHashCode())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public override string ToString()
        {
            return $"Name: {Name}" + "\n" 
                   + $"ID: {ID}" + "\n"
                   + $"Age: {Age}" + "\n"
                   + $"SSN: {SocialSecurityNumber}";
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode() + ID.GetHashCode() + Age.GetHashCode() + SocialSecurityNumber.GetHashCode();
        }
    }
}
