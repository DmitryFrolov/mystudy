﻿using System;

namespace lessons.lesson_11.Models
{
    public class SalesPerson : Employee
    {
        public SalesPerson(string fullName, int age, int empID, float currPay, string ssn, int numbOfSales)
            : base(fullName, age, empID, currPay, ssn)
        {
            SalesNumber = numbOfSales;
        }

        public int SalesNumber { get; set; }

        public sealed override void GiveBonus(float amount)
        {
            int salesBonus = 0;
            if (SalesNumber >= 0 && SalesNumber <= 100)
                salesBonus = 10;
            else
            {
                if (SalesNumber >= 101 && SalesNumber <= 200)
                    salesBonus = 15;
                else
                    salesBonus = 20;
            }
            
            base.GiveBonus(amount * salesBonus);
        }

        public sealed override int CalculateHolidays()
        {
            int additionalHolidays = SalesNumber < 10 ? SalesNumber : 10;

            return Holidays = base.CalculateHolidays() + additionalHolidays;
        }

        public override string ToString()
        {
            return $"Type: {nameof(SalesPerson)}" + "\n" 
                                                  + base.ToString() + "\n" 
                                                  + $"Number of sales: {SalesNumber}";
        }
    }
}
