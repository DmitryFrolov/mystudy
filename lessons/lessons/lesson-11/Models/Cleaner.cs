﻿namespace lessons.lesson_11.Models
{
    public class Cleaner : Employee
    {
        public Cleaner(string name, int age, int id, float pay, string ssn = "") 
            : base(name, age, id, pay, ssn)
        {
        }

        public override void DisplayStats()
        {
            base.DisplayStats();
        }
    }
}