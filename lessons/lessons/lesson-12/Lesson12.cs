﻿using System;
using lessons.lesson_12.Interfaces;
using lessons.lesson_12.Models;

namespace lessons.lesson_12
{
    public enum Target
    {
        Window,
        Console
    }

    public class MyConsole : ITargetPrintable
    {
        public void Print(string message)
        {
            Console.WriteLine(message);
        }
    }

    public class MyWindow : ITargetPrintable
    {
        public void Print(string message)
        {
            //MessageBox.Show(message);
        }
    }

    public static class Lesson12
    {
        private static ITargetPrintable placeToPrint;

        public static void Run()
        {
            SetOutputTarget(Target.Console);

            placeToPrint.Print("some message");

            Console.WriteLine("***** The Employee Class Hierarchy *****\n");

            var fran = new SalesPerson("Fran", 150, 93, 3000, "932-32-3232", 31);
            var chucky = new Manager("Chucky", 50, 92, 100000, "333-23-2322", 9000);
            var bred = new PTSalesPerson("Bred", 50, 92, 100000, "333-23-2322", 9000);

            Employee[] employees = {fran, bred, chucky};

            var circle = new Circle();
            var rectangle = new Rectangle();

            Shape[] shapes = {circle, rectangle};

            //IPrintable[] printables = {fran, bred, chucky, circle, rectangle};
            //IDisplayable[] displayables = {circle, rectangle};

            ICloneable[] clonables = {fran, bred, chucky, circle, rectangle};

            //foreach (var item in printables)
            //{
            //    PrintObject(item);
            //}
        }

        private static void SetOutputTarget(Target target)
        {
            switch (target)
            {
                case Target.Window:
                    placeToPrint = new MyWindow();
                    break;

                case Target.Console:
                    placeToPrint = new MyConsole();
                    break;
            }
        }

        public static IPrintable PrintObject(IPrintable printable)
        {
            //Console.WriteLine(printable.Print());
            Console.WriteLine();

            return printable;
        }

        public static object GetClone(ICloneable cloneable)
        {
            return cloneable.Clone();
        }
    }
}
