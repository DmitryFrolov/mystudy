﻿using System;

namespace lessons.lesson_12.Interfaces
{
    public interface IPrintable
    {
        string Print();
    }
    
    public interface ITargetPrintable
    {
        void Print(string message);
    }

    public interface IGettingStringable
    {
        string GetString();
    }

    public interface IDisplayable : IPrintable, IGettingStringable
    {
        void Display();
    }
}