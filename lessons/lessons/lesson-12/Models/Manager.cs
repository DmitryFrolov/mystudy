﻿using System;

namespace lessons.lesson_12.Models
{
    public class Manager : Employee
    {
        public Manager(string fullName, int age, int empID, float currPay, string ssn, int numbOfOpts)
            : base(fullName, age, empID, currPay, ssn)
        {
            // This property is defined by the Manager class.
            StockOptions = numbOfOpts;
        }

        public int StockOptions { get; set; }

        public override void GiveBonus(float amount)
        {
            base.GiveBonus(amount);
            Random r = new Random();
            StockOptions += r.Next(500, 1000);
        }

        public override void DisplayStats()
        {
            Console.WriteLine($"Type: {nameof(Manager)}");
            base.DisplayStats();
            Console.WriteLine("Number of Stock Options: {0}", StockOptions);
        }

        public override object Clone()
        {
            return new Manager(Name, Age, ID, Pay, SocialSecurityNumber, StockOptions);
        }
    }
}