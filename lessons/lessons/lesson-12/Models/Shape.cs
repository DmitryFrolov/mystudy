﻿using System;
using lessons.lesson_12.Interfaces;

namespace lessons.lesson_12.Models
{
    public class Shape : Object, ICloneable, IPrintable
    {
        public virtual string ShapeName { get; }

        public Shape()
        {
            ShapeName = "Base Shape";
        }

        public virtual string Print()
        {
            return ShapeName;
        }

        public object Clone()
        {
            return new Shape();
        }
    }

    public class Circle : Shape
    {
        public override string ShapeName { get; }

        public Circle()
        {
            ShapeName = "Circle Shape";
        }

        public override string Print()
        {
            return base.Print() + " from circle.";
        }
    }

    public class Rectangle : Shape
    {
        public override string ShapeName { get; }

        public Rectangle()
        {
            ShapeName = "Rectangle Shape";
        }
    }
}
