﻿using System;
using lessons.lesson_08.Models;

namespace lessons.lesson_08
{
    public class Lesson08
    {
        public static void Run()
        {
            // Allocate and configure a Car object.
            Car myCar1 = new Car("name0");
            Car myCar2 = new Car("name1");
            Car myCar3 = new Car("name2");
            Car myCar4 = new Car("name3");

            Console.ReadLine();

            myCar1.PrintState();
            myCar2.PrintState();
            myCar3.PrintState();
            myCar4.PrintState();

            Console.WriteLine("***** Fun with Class Types *****\n");

            // Make a Motorcycle with a rider named Tiny?
            Motorcycle c = new Motorcycle(5);
            c.SetDriverName("Tiny");
            c.PopAWheely();
            Console.WriteLine("Rider name is {0}", c.driverName); // Prints an empty name value!

            Console.WriteLine();

            // Allocate and configure a Car object.
            Car myCar = new Car();
            myCar.PetName = "Henry";
            myCar.CurrentSpeed = 10;

            // Speed up the car a few times and print out the
            // new state.
            for (int i = 0; i <= 10; i++)
            {
                myCar.SpeedUp(5);
                myCar.PrintState();
            }
            Console.ReadLine();

            //Console.WriteLine("***** Fun with Const *****\n");
            //Console.WriteLine("The value of PI is: {0}", MyMathClass.PI);
        }

        #region Helper function to make some motor bikes...
        static void MakeSomeBikes()
        {
            // driverName = "", driverIntensity = 0  
            Motorcycle m1 = new Motorcycle();
            Console.WriteLine("Name= {0}, Intensity= {1}",
                m1.driverName, m1.driverIntensity);

            // driverName = "Tiny", driverIntensity = 0 
            Motorcycle m2 = new Motorcycle(name: "Tiny");
            Console.WriteLine("Name= {0}, Intensity= {1}",
                m2.driverName, m2.driverIntensity);

            // driverName = "", driverIntensity = 7  
            Motorcycle m3 = new Motorcycle(7);
            Console.WriteLine("Name= {0}, Intensity= {1}",
                m3.driverName, m3.driverIntensity);
        }

        #endregion
    }
}
