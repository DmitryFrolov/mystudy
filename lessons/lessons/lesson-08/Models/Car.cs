﻿using System;
using System.Xml;

namespace lessons.lesson_08
{
    class Car
    {
        public string PetName; //fields
        public int CurrentSpeed { get; set; } // property

        private const int MaxSpeed = 160;

        public static readonly string CarType;

        static Car()
        {
            Console.WriteLine("in static constructor");

            CarType = "small car";
        }

        public Car(string PetName = "", int CurrentSpeed = 10) // constructor
        {
            Console.WriteLine("in master constructor");

            this.PetName = PetName;

            if (CurrentSpeed > MaxSpeed)
                CurrentSpeed = MaxSpeed;

            this.CurrentSpeed = CurrentSpeed;
        }

        public void PrintState() // method
        {
            Console.WriteLine($"{PetName} is going {CurrentSpeed} MPH. Type: {CarType}");
        }

        public void SpeedUp(int delta) // method
        {
            if (CanSpeedBeIncreased(delta))
            {
                CurrentSpeed += delta;
            }

            if (delta > 100)
            {

            }
        }

        private bool CanSpeedBeIncreased(int delta)
        {
            if (CurrentSpeed > MaxSpeed && CurrentSpeed + delta < MaxSpeed)
            {
                return false;
            }

            return true;
        }
    }
}