﻿
namespace lessons.lesson02
{
    public class HomeWork02
    {
        public void Run()
        {

        }

        // все параметры функции изначально должны быть получены от ввода пользователя на консоль.
        // Преобразованы в числовые значения и выведен результат обратно на консоль

        static int GetSum(int a, int b)
        {
            return 0;
        }

        static float GetSum(float a, float b)
        {
            return 0;
        }

        static double GetSum(double a, double b)
        {
            return 0;
        }

        static double GetAverage(int a, int b, int c)
        {
            return 0;
        }

        static double GetDiscriminant(int a, int b, int c)
        {
            return 0;
        }

        // Теорема Пифагора: получение гипотенузы (с)
        static double GetHypotenuse(double a, double b)
        {
            return 0;
        }

        // Теорема Пифагора: получение катета, при изместной кипотенузе и втором катете (с)
        static double GetLeg(double a, double b)
        {
            return 0;
        }
    }
}
