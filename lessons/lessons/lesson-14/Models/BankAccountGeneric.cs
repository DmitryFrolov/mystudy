﻿namespace lessons.lesson_14.Models
{
    public class BankAccountGeneric<T, T1> : IBankAccount<T>
    {
        public T ID { get; set; }

        public string Name { get; set; }

        public T1 AmountOfMoney { get; set; }

        public BankAccountGeneric(T id = default)
        {
            ID = id;
            Name = default;
            AmountOfMoney = default;
        }

        public BankAccountGeneric(T id, string name, T1 amountOfMoney)
        {
            ID = id;
            Name = name;
            AmountOfMoney = amountOfMoney;
        }

        public T GetID()
        {
            return ID;
        }

        public void SetID(T id)
        {
            ID = id;
        }

        public void SendMoney(IBankAccount<T> bankAccount)
        {
            var anotherAccount = new BankAccountGeneric<T, T1>(bankAccount.ID);
        }
    }
}
