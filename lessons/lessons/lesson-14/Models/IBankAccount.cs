﻿namespace lessons.lesson_14.Models
{
    public interface IBankAccount<T>
    {
        T ID { get; set; }

        void SendMoney(IBankAccount<T> bankAccount);
    }
}