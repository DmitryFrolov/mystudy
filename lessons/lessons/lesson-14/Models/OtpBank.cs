﻿using System;

namespace lessons.lesson_14.Models
{
    public class OtpBank : BankAccountGeneric<Guid, int>, IBankAccount<Guid>
    {
        public OtpBank()
            : base()
        {
        }

        public OtpBank(Guid id, string name, int amountOfMoney) 
            : base(id, name, amountOfMoney)
        {
        }
    }
}
