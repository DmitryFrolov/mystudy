﻿
namespace lessons.lesson_14.Models
{
    public static class BankAccountHelper
    {
        public static void SendMoney<T>(T account1, T account2) 
            where T : Privat24Bank, new()
        {
            account1.SendMoney(account2);
        }

        public static T CreateBankAccount<T>() where T : new()
        {
            return new T();
        }

        public static T CreateSampleStructure<T>() where T: class, new()
        {
            return new T();
        }
    }
}
