﻿using System;

namespace lessons.lesson_14.Models
{
    public static class MyMath
    {
        public static void Print<T0, T1>(T0 first, T1 second)
        {
            Console.WriteLine(first);
            Console.WriteLine(second);
        }

        //public static void Print(int first, int second)
        //{
        //    Console.WriteLine(first);
        //    Console.WriteLine(second);
        //}

        //public static void Print(string first, string second)
        //{
        //    Console.WriteLine(first);
        //    Console.WriteLine(second);
        //}

        //public static void Print(double first, double second)
        //{
        //    Console.WriteLine(first);
        //    Console.WriteLine(second);
        //}
    }
}
