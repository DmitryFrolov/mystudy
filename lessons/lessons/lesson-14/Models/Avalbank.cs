﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lessons.lesson_14.Models
{
    public class AvalBank : BankAccountGeneric<string, decimal>, IBankAccount<string>
    {
        public AvalBank()
        {
                
        }

        public AvalBank(string id, string name, decimal amountOfMoney) 
            : base(id, name, amountOfMoney)
        {
        }
    }
}
