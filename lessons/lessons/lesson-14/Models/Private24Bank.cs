﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lessons.lesson_14.Models
{
    public class Privat24Bank : BankAccountGeneric <int, double>, IBankAccount<int>
    {
        public Privat24Bank()
        {
                
        }

        public Privat24Bank(int id, string name, double amountOfMoney) 
            : base(id, name, amountOfMoney)
        {
        }

        public static Privat24Bank operator +(Privat24Bank bank1, Privat24Bank bank2)
        {
            return new Privat24Bank(bank1.GetID(), bank1.Name, bank1.AmountOfMoney + bank2.AmountOfMoney);
        }
    }
}
