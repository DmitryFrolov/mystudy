﻿using System;
using lessons.lesson_14.Models;

namespace lessons.lesson_14
{
    public class Lesson14
    {
        public static void Run()
        {
            Privat24Bank account = new Privat24Bank(13222, "Adam", 130000);
            Privat24Bank pb2 = new Privat24Bank(13222, "Sem", 130000);

            AvalBank account2 = new AvalBank("tur232-34s", "Simon", 130000);
            OtpBank account3 = new OtpBank(Guid.NewGuid(), "Josh", 100000);
            OtpBank account4 = new OtpBank();

            var i = account.GetID();
            var a = account2.GetID();
            var g = account3.GetID();

            //var newBankAccount = account + pb2;

            Guid newguid = Guid.NewGuid();

            account3.SetID(newguid);


            MyMath.Print<int, int>(5, 10);
            MyMath.Print<string, int>("hello", 10);
            MyMath.Print<double, double>(10.0, 10.4);


            BankAccountHelper.SendMoney<Privat24Bank>(account, pb2);

            var acc = BankAccountHelper.CreateBankAccount<Privat24Bank>();
            var acc2 = BankAccountHelper.CreateBankAccount<AvalBank>();
            var acc3 = BankAccountHelper.CreateBankAccount<OtpBank>();
        }
    }
}
