﻿using System;
using System.Dynamic;

namespace lessons.lesson_09.Models
{
    class Rectangle
    {
        public Point TopLeft { get; set; }

        public Point BottomRight { get; set; }

        public Rectangle(Point topLeft, Point bottomRight)
        {
            TopLeft = topLeft;
            BottomRight = bottomRight;
        }

        public void DisplayStats()
        {
            Console.WriteLine("[TopLeft: {0}, {1}, {2} BottomRight: {3}, {4}, {5}]",
                TopLeft.X, TopLeft.Y, TopLeft.Color,
                BottomRight.X, BottomRight.Y, BottomRight.Color);
        }
    }
}
