﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata.Ecma335;
using System.Text;

namespace lessons.lesson_09.Models
{
    public abstract class Animal
    {
        private bool IsAlive { get; set; }

        protected abstract string TypeName { get; }
        protected abstract int MaxAge { get; }

        public double Weight { get; set; }
        public int Age { get; set; }

        public abstract void SayHello();

        public void Jump()
        {
            Console.WriteLine("Im jumping now.");
        }

        public Animal()
        {
            Console.WriteLine("Animal ctor invoked (without parameters)");
        }

        public Animal(double weight, bool isAlive, int age)
        {
            Console.WriteLine("Animal ctor invoked");

            Weight = weight;
            IsAlive = isAlive;
            Age = age;
        }

        public Animal GetAnimal()
        {
            return this;
        }

    }

    public abstract class Bird : Animal
    {
        public double MaxFlySpeed { get; set; }

        protected double FlyLength { get; set; }

        protected Bird()
        {
            Console.WriteLine("Bird ctor invoked (without parameters)");
        }

        public void Fly()
        {
            Console.WriteLine($"{TypeName} is flying");
        }

        public virtual double GetFlyLength()
        {
            return 10.0;
        }

        public string GetTypeName()
        {
            return TypeName;
        }
    }

    public class Dove : Bird
    {
        protected override string TypeName => "Dove";
        protected override int MaxAge => 3;

        public bool IsSoapable { get; set; }

        public Dove()
        {
            Console.WriteLine("Dove ctor invoked (without parameters)");
        }

        public override double GetFlyLength()
        {
            return 10.0 * 3;
        }

        public override void SayHello()
        {
            Console.WriteLine($"{TypeName}: kurlic-kurlic-kurlic");
        }
    }

    public class Chicken : Bird
    {
        protected override string TypeName => "Chicken";
        protected override int MaxAge => 2;

        public override double GetFlyLength()
        {
            return 0;
        }

        public override void SayHello()
        {
            Console.WriteLine($"{TypeName}: ko-ko-ko");
        }
    }

    public class Human : Animal
    {
        protected override string TypeName => "Human";
        protected override int MaxAge => 100;

        public override void SayHello()
        {
            Console.WriteLine($"{TypeName}: hi there");
        }
    }

    public class Tiger
    {
        
    }
}
