﻿using System;
using lessons.lesson_09.Models;

namespace lessons.lesson_09
{
    public static class Lesson09
    {
        public static void Run()
        {
            Console.WriteLine("***** Fun with Object Init Syntax *****\n");

            // Make a Point by setting each property manually.
            Point firstPoint = new Point();
            firstPoint.X = 10;
            firstPoint.Y = 10;
            firstPoint.Color = PointColor.Gold;

            firstPoint.DisplayStats();
            Console.WriteLine();

            // Or make a Point via a custom constructor.
            Point anotherPoint = new Point(20, 20);
            anotherPoint.DisplayStats();
            Console.WriteLine();

            // Or make a Point using object init syntax.
            Point finalPoint = new Point() { X = 30, Y = 30, Color = PointColor.Gold };
            finalPoint.DisplayStats();
            Console.WriteLine();

            var topRight = new Point {X = 30, Y = 30, Color = PointColor.Gold};
            var bottomRight = new Point {X = 30, Y = 30, Color = PointColor.Gold};

            var rect = new Rectangle(topRight, bottomRight);


            //// Calling a more interesting custom constructor with init syntax.
            //Point goldPoint = new Point(PointColor.Gold) { X = 90, Y = 20 };
            //goldPoint.DisplayStats();
            //Console.WriteLine();

            //// Create and initialize a Rectangle.
            //Rectangle myRect = new Rectangle
            //{
            //    TopLeft = new Point { X = 10, Y = 10 },
            //    BottomRight = new Point { X = 200, Y = 200 }
            //};
            //myRect.DisplayStats();
            //Console.WriteLine();

            //Console.ReadLine();
        }
    }
}
