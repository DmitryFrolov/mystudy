﻿using System;

namespace Lesson7ClassLibrary
{
    internal class MyAwesomeClass
    {
        public void DisplayHello()
        {
            Console.WriteLine("Hello from MyAwesomeClass");
        }
    }
}
